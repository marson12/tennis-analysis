#
#
# NOTE
# filtering for years is done in the getData file as it is faster to
# skip the entire data file for a year then it is to get all the data from that
# file and then remove it. Other filter items cannot be seperated as easily because
# the filter tennis_atp-master has a different file per year
#
# This is also not ment to filter out invalid data for each match,
# rather it will filter for matches that have a specific criteria
#


def filterCourts(matchYears, courtTypes):
    # courtTypes is courts to be included
    # note thawt some old matches have the type '' empty string
    filteredMatchYears = []
    for year in matchYears:
        filteredYear = []
        for match in year:
            if match['surface'] in courtTypes:
                filteredYear.append(match)
        filteredMatchYears.append(filteredYear)
    return filteredMatchYears

