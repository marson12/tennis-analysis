def checkAddPlayer(players, player):
    if player not in players:
        players[player] = {'wins': 0, 'played': 0, 'name': player}


def updatePlayer(players, player, wins, played):
    players[player]['wins'] += wins
    players[player]['played'] += played


def highestMatchWinRate(matchYears):
    players = {}
    for year in matchYears:
        for match in year:
            winner = match['winner_name']
            loser = match['loser_name']
            checkAddPlayer(players, winner)
            checkAddPlayer(players, loser)
            updatePlayer(players, winner, 1, 1)
            updatePlayer(players, loser, 0, 1)

    playersList = []
    for key in players.keys():
        player = players[key]
        if player['played'] > 50:
            playersList.append(player)
            setWinRate = player['wins'] / player['played']
            player['winRate'] = setWinRate

    playersList.sort(key=lambda x: x['winRate'], reverse=True)
    return playersList


def highestSetWinRateSingles(matchYears):
    players = {}
    for year in matchYears:
        for match in year:
            winner = match['winner_name']
            loser = match['loser_name']
            sets = match['score'].split(' ')
            checkAddPlayer(players, winner)
            checkAddPlayer(players, loser)
            # consider the case where someone leaves in the middle of the match
            winnerSets = 0
            loserSets = 0
            for set in sets:
                scores = set.split('-')
                try:
                    if '(' in scores[1]:
                        scores[1] = scores[1].split('(')[0]
                    if int(scores[0]) > int(scores[1]):
                        winnerSets += 1
                    else:
                        loserSets += 1
                except:
                    # the usual culprit for this is set=='RET'
                    pass

            updatePlayer(players, winner, winnerSets, winnerSets + loserSets)
            updatePlayer(players, loser, loserSets, winnerSets + loserSets)

    playersList = []
    for key in players.keys():
        player = players[key]
        if player['played'] > 50:
            playersList.append(player)
            setWinRate = player['wins'] / player['played']
            player['winRate'] = setWinRate

    playersList.sort(key=lambda x: x['winRate'], reverse=True)
    return playersList


def highestWinRateSinglesPerYear(matchYears, isSetWinrate):
    playersYearList = []
    bestOfYears = []
    for matches in matchYears:
        if isSetWinrate:
            playersYear = highestSetWinRateSingles([matches])
        else:
            playersYear = highestMatchWinRate([matches])

        bestofYear = {'winRate': 0}
        # get tournament year, which is the year the set was in.
        year = matches[0]['tourney_id'].split('-')[0]
        for player in playersYear:
            player['year'] = year
            if player['winRate'] > bestofYear['winRate']:
                bestofYear = player
            playersYearList.append(player)
        bestOfYears.append(bestofYear)

    # can also sort by 'year'
    bestOfYears.sort(key=lambda x: x['winRate'], reverse=True)
    playersYearList.sort(key=lambda x: x['winRate'], reverse=True)
    return playersYearList
