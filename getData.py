import csv
import os
import re


# expressions
reAtpMatchesSingles = 'atp_matches_[0-9]{4}.csv'
reFutures = 'atp_matches_futures_[0-9]{4}.csv'
rechallengers = 'atp_matches_qual_chall_[0-9]{4}.csv'


def atpMatchesSinglesAllYears():
    return atpMathesSinglesByYears(0, 9999)


def atpMathesSinglesByYears(start, end):
    return getMatchesFilesByRegularExpressionsAndYear(reAtpMatchesSingles, start, end)


def futuresSinglesByYears(start, end):
    return getMatchesFilesByRegularExpressionsAndYear(reFutures, start, end)


def challengerSinglesByYears(start, end):
    return getMatchesFilesByRegularExpressionsAndYear(rechallengers, start, end)


def getMatchesFilesByRegularExpressionsAndYear(regularExpression, start=None, end=None, ):
    yearSets = []
    folder_path = 'tennis_atp-master'
    files = os.listdir(folder_path)

    # find the files. filter the files name based on regularExpression and the year in that fileName
    filteredFiles = []
    for fileName in files:
        if re.match(regularExpression, fileName):
            if start and end:
                year = int(re.search('[0-9]{4}', fileName).group(0))
                if start <= year <= end:
                    filteredFiles.append(fileName)
            else:
                filteredFiles.append(fileName)

    for fileName in filteredFiles:
        yearSet = []
        path = os.path.join(folder_path, fileName)
        with open(path, 'r') as f:  # open the file
            reader = csv.reader(f)
            keys = next(reader)

            for row in reader:
                match = dict(zip(keys, row))
                yearSet.append(match)
        yearSets.append(yearSet)
    return yearSets