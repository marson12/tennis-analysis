import getData
import analyzeData
import filterData


def displayData(data, displayLimit=10, reverse=False):
    count = 0
    if reverse:
        while count < displayLimit:
            print(str(data[count]) + '       ' + str(-1 * (count + 1)))
            count += 1
    else:
        while count < displayLimit:
            print(str(data[count]) + '      ' + str(count + 1))
            count += 1


startyear = 0
endyear = 9999
# surfaces = ['Grass', 'Clay', 'Carpet', 'Hard', '']
surfaces = ['Carpet']

matchYears = getData.atpMatchesSinglesAllYears()
matchYears = filterData.filterCourts(matchYears, surfaces)
analyzedMatches = analyzeData.highestMatchWinRate(matchYears)
displayData(analyzedMatches, 70)
